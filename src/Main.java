import processing.core.PApplet;
import processing.core.PVector;

public class Main extends PApplet {
	public static PApplet processing;

	public Game game = new Game(new PVector(50, 50), 600, 600);

	public static void main(String[] args) {
		PApplet.main("Main", args);
	}

	public void setup() {
		processing = this;
		frameRate(90);
		game.createGround();
		game.getGround().generateWall(10,40);
		game.getGround().generateUnit(80, 25);
	}

	public void settings() {
		size(700, 700);
	}

	public void draw() {
		noStroke();
		background(255);
		game.display();
		game.getGround().update();

		//Debug
		if (keyPressed) {
			if (key == 'D' || key == 'd') {
				debug();
			}
 		}
	}

	public void debug() {

		Main.processing.fill(152, 97, 97);
		Main.processing.textSize(18);
		String mousePos = "X : " + mouseX + " ; Y : " + mouseY;
		Main.processing.text(mousePos, mouseX - 60, mouseY - 10);

		for (int i = 0 ; i < game.getGround().getUnitList().size() ; i++) {
			PVector waypoint = game.getGround().getUnitList().get(i).getWaypoint();
			PVector position = game.getGround().getUnitList().get(i).getPosition();
			int diameter = game.getGround().getUnitList().get(i).getHitbox();
			PVector location = game.getGround().getUnitList().get(i).getLocation();
			float shade = game.getGround().getUnitList().get(i).getShade();
			boolean isFollowed = game.getGround().getUnitList().get(i).getIsFollowed();
			boolean isFollowing = game.getGround().getUnitList().get(i).getIsFollowing();

			Main.processing.fill(255, 0, 0);
			Main.processing.circle(waypoint.x, waypoint.y, 8);

			Main.processing.stroke(255, 0, 0);
			Main.processing.line(location.x, location.y, position.x, position.y);
			Main.processing.noStroke();

			Main.processing.textSize(10);
			String posStr = "X : " + (int) position.x + " ; Y : " + (int) position.y;
			Main.processing.text(posStr, position.x - (posStr.length() * 2f), position.y - diameter);

			Main.processing.noFill();
			Main.processing.stroke(255, 0, 0);

			// En vert les leaders
			if (isFollowed) {
				Main.processing.stroke(0, 255, 0);
			}

			// En bleu les suiveurs
			if (isFollowing) {
				Main.processing.stroke(0, 0, 255);
			}

			Main.processing.circle(position.x, position.y, diameter);

			Main.processing.noStroke();
			Main.processing.fill(shade, 90, 200);
			Main.processing.circle(location.x,location.y,10);
		}
	}
}
