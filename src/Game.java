import processing.core.PVector;

public class Game {
	private PVector posBegin;
	private int sizeX;
	private int sizeY;
	private Ground ground;

	public Game (PVector posBegin, int sizeX, int sizeY) {
		this.posBegin = posBegin;
		this.sizeX = sizeX;
		this.sizeY = sizeY;
	}

	/**
	 * Crée un terrain
	 */
	public void createGround() {
		this.ground = new Ground(posBegin, this.sizeX, this.sizeY);
	}

	/**
	 * Affiche le terrain et les unités et murs associés
	 */
	public void display() {
		this.ground.display();
	}

	public Ground getGround() {
		return this.ground;
	}
}
