import processing.core.PVector;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class Ground {
	private final PVector posBegin;
	private final int sizeX;
	private final int sizeY;
	private ArrayList<Wall> wallList;
	private ArrayList<Unit> unitList;

	/**
	 * Crée un terrain
	 * @param posBegin position de création du terrain
	 * @param sizeX longueur
	 * @param sizeY hauteur
	 */
	public Ground(PVector posBegin, int sizeX, int sizeY) {
		this.posBegin = posBegin;
		this.sizeX = sizeX;
		this.sizeY = sizeY;
		unitList = new ArrayList<>();
		wallList = new ArrayList<>();
	}

	/**
	 * Génère des murs aléatoirement sur le terrain
	 * @param nbWalls nombre de murs à générer
	 * @param diameter diamètre des murs
	 */
	public void generateWall(int nbWalls, int diameter) {
		for (int i = 0 ; i < nbWalls ; i++) {
			PVector location = new PVector(ThreadLocalRandom.current().nextInt((int) this.posBegin.x + diameter / 2, (int) this.posBegin.x + this.sizeX - diameter / 2), ThreadLocalRandom.current().nextInt((int) this.posBegin.y + diameter / 2, (int) this.posBegin.y + this.sizeY - diameter / 2));

			while (isValidLocation(location, diameter)) {
				location = new PVector(ThreadLocalRandom.current().nextInt((int) this.posBegin.x + diameter / 2, (int) this.posBegin.x + this.sizeX - diameter / 2), ThreadLocalRandom.current().nextInt((int) this.posBegin.y + diameter / 2, (int) this.posBegin.y + this.sizeY - diameter / 2));
			}

			wallList.add(new Wall(location, diameter));
		}
	}

	/**
	 * Génère des unités sur le terrain
	 * @param nbUnit nombre d'unités à générer
	 * @param hitbox zone de collision des unités
	 */
	public void generateUnit(int nbUnit, int hitbox) {
		for (int i = 0 ; i < nbUnit ; i++) {
			PVector location = new PVector(ThreadLocalRandom.current().nextInt((int) this.posBegin.x, (int) this.posBegin.x + this.sizeX), ThreadLocalRandom.current().nextInt((int) this.posBegin.y, (int) this.posBegin.y + this.sizeY));

			while (isValidLocation(location, hitbox)) {
				location = new PVector(ThreadLocalRandom.current().nextInt((int) this.posBegin.x, (int) this.posBegin.x + this.sizeX), ThreadLocalRandom.current().nextInt((int) this.posBegin.y, (int) this.posBegin.y + this.sizeY));
			}

			unitList.add(new Unit(location, this, hitbox));
		}
	}

	/**
	 * Renvoi une unité proche d'une position
	 * @param position position à tester
	 * @param dist distance à partir de laquelle une unité est considérée comme proche
	 * @return renvoi l'unité proche ou null
	 */
	public Unit nearAnUnit(PVector position, int dist) {
		for (Unit unit : unitList) {
			if (position.dist(unit.getPosition()) < dist && position.dist(unit.getPosition()) != 0) {
				return unit;
			}
		}

		return null;
	}

	/**
	 * Permet de savoir si une position est valide (dans le terrain et pas dans un mur)
	 * @param location position à tester
	 * @param hitbox taille de la zone de collision
	 * @return renvoi vrai si la position est valide, faux si elle est invalide
	 */
	public boolean isValidLocation(PVector location, int hitbox) {
		return !isInGround(location) || isInAWall(location, hitbox) != null;
	}

	/**
	 * Permet de savoir si une position est dans le terrain
	 * @param location position à tester
	 * @return renvoi vrai si la position est dans le terrain, faux dans le cas contraire
	 */
	public boolean isInGround(PVector location) {
		return location.x >= this.posBegin.x && location.x <= this.posBegin.x + this.sizeX && location.y >= this.posBegin.y && location.y <= this.posBegin.y + this.sizeY;
	}

	/**
	 * Permet de savoir si une position est dans un mur
	 * @param location position à tester
	 * @param hitbox taille de la zone de collision
	 * @return renvoi vrai si la position est dans un mur
	 */
	public Wall isInAWall(PVector location, int hitbox) {
		for (Wall wall : this.wallList) {
			if (location.dist(wall.getPosition()) < (wall.getDiameter() / 2f + hitbox / 2f)) {
				return wall;
			}
		}

		return null;
	}

	/**
	 * Affiche le terrain et les positions des murs et des unités
	 */
	public void display() {
		Main.processing.fill(209, 245, 211);
		Main.processing.rect(this.posBegin.x, this.posBegin.y, this.sizeX, this.sizeY);

		for (Wall wall : wallList) {
			wall.display();
		}
		for (Unit unit : unitList) {
			unit.display();
		}
	}

	/**
	 * Actualisation de la position des unités en temps réel
	 */
	public void update() {
		// Debug
		//PVector newLocation = new PVector(Main.processing.mouseX, Main.processing.mouseY);

		for (Unit unit : unitList) {
			unit.move();
		}
	}

	public ArrayList<Unit> getUnitList() {
		return this.unitList;
	}

	public ArrayList<Wall> getWallList() {
		return this.wallList;
	}

	public PVector getPosBegin() {
		return this.posBegin;
	}

	public int getSizeX() {
		return this.sizeX;
	}

	public int getSizeY() {
		return this.sizeY;
	}
}
