import processing.core.PVector;
import java.util.concurrent.ThreadLocalRandom;
import static processing.core.PApplet.*;

public class Unit {
	private PVector position; // Position actuelle
	private Ground ground; // terrain
	private PVector waypoint; // walker destination
	private PVector direction;
	private PVector acceleration;
	private float maxSpeed; // Vitesse maximum
	private int hitbox; // Taille réelle de l'unité
	private float shade; // Couleur
	private float originalShade; // Couleur originale
	private PVector location; // new location where to go
	private Unit unitFollowed;
	private boolean isFollowed; // est suivi (leader)
	private boolean isFollowing; // Suit une unité
	private boolean hasPriority; // à la priorité (n'a pas à changer de trajectoire s'il rencintre une unité)

	/**
	 * Crée une unité : un agent pouvant suivre d'autres agents ou se balader sur le terrain en évitant des obstacles
	 * @param position position de l'unité
	 * @param ground terrain sur lequel elle se trouve
	 * @param hitbox taille de la zone de collision (diamètre)
	 */
	public Unit(PVector position, Ground ground, int hitbox) {
		this.position = position;
		this.ground = ground;
		this.hitbox = hitbox;
		this.waypoint = this.position;
		this.direction = this.position;

		// Couleur aléatoire entre le rose et le bleu
		this.originalShade = Main.processing.random(255);
		this.shade = this.originalShade;

		this.acceleration = new PVector(-0.001f,0.1f);
		this.maxSpeed = 2f;

		this.isFollowed = false;
		this.isFollowing = false;

		this.location = new PVector(0,0);
		this.location = findANewLocation();

		this.hasPriority = false;
	}

	/**
	 * Fait avancer l'unité
	 */
	public void move() {
		// destination
		PVector findPath = findPath(this.location);

		// Debugger
		this.waypoint = findPath;

		// Vecteur vitesse
		PVector velocity = PVector.sub(findPath, this.position); // direction

		velocity.normalize(); // taille du vecteur 1
		velocity.mult(1f); // vitesse

		this.direction = velocity; // direction

		if (isFollowing && this.position.dist(unitFollowed.position) < this.hitbox) {
			this.maxSpeed = 0.5f;
		} else {
			this.maxSpeed = 2f;
		}

		velocity.add(this.acceleration);
		velocity.limit(this.maxSpeed);

		this.position.add(velocity); // déplacement

		this.location = findANewLocation();
	}

	/**
	 * Trouve un chemin pour atteindre l'endroit voulu
	 * @param location endroit où aller
	 * @return point d'évitement s'il faut éviter un obstacle, endroit où aller sinon
	 */
	private PVector findPath(PVector location) {
		PVector walker = new PVector(this.position.x, this.position.y); // parcours le chemin en ligne droite
		PVector direction = PVector.sub(location, walker); // direction

		direction.normalize();
		direction.mult(1f); // vitesse du walker

		while (walker.dist(location) > 1f) {
			walker.add(direction);

			Wall wallToAvoid = this.ground.isInAWall(walker, this.hitbox); // Mur proche
			Unit unitToAvoid = this.ground.nearAnUnit(this.position, this.hitbox); // Unité à une distance de moins d'une hitbox

			// Si rencontre un mur
			if (wallToAvoid != null) {
				return avoidWall(walker, wallToAvoid);
			}

			// S'il suit une unité, qu'il a des amis et qu'une unité doit être évitée (moins d'une hitbox)
			if (unitToAvoid != null) {
				if (isFollowed) {
					this.hasPriority = true;
				}

				// S'il n'a pas la priorité
				if (!hasPriority) {
					unitToAvoid.hasPriority = true;
					return avoidFriends(walker, unitToAvoid);
				}
			}
		}

		return walker;
	}

	/**
	 * Trouve la meilleure normale à la trajectore unit-location à l'endroit où le walker a rencontré un obstacle (unité ou mur)
	 * @param walker fait la trajectoire avant que l'unité ne soit en mouvement pour détecter les obstacles et les éviter
	 * @param normal formule de la normale
	 * @param unitObstacleDirection vecteur allant de l'unité à l'obstacle (unité ou mur)
	 */
	private void normalCalculation(PVector walker, PVector normal, PVector unitObstacleDirection) {
		PVector unitLocationDirection = PVector.sub(this.location, walker);

		// Atan2 permet de trouver l'angle entre deux vecteurs (0-2PI)
		float angle = atan2(unitLocationDirection.y, unitLocationDirection.x) - atan2(unitObstacleDirection.y, unitObstacleDirection.x);

		if (angle < 0) {
			angle += TWO_PI;
		}

		// Normale en fonction de la direction de l'unité par rapport à l'obstacle
		if (angle < PI) {
			walker.add(normal);
		} else {
			walker.add(normal.mult(-1));
		}

		// Si le point d'évitement trouvé est non-valide on prend l'autre
		if (this.ground.isValidLocation(walker, this.hitbox)) {
			walker.mult(-1);
		}
	}

	/**
	 * Trouve une trajectoire pour éviter une unité
	 * @param walker fait la trajectoire avant que l'unité ne soit en mouvement pour détecter les obstacles et les éviter
	 * @param unitToAvoid Unité à éviter
	 * @return renvoi le point d'évitement trouvé
	 */
	private PVector avoidFriends(PVector walker, Unit unitToAvoid) {
		PVector walkerToLocation = PVector.sub(location, walker);
		walkerToLocation.normalize();

		PVector normal = new PVector(-walkerToLocation.y, walkerToLocation.x);

		normal.mult(unitToAvoid.hitbox / 2f + this.hitbox / 2f);

		PVector unitUnitDirection = PVector.sub(unitToAvoid.position, walker);

		normalCalculation(walker, normal, unitUnitDirection);

		return walker;
	}

	/**
	 * Trouve une trajectoire pour éviter un mur
	 * @param walker fait la trajectoire avant que l'unité ne soit en mouvement pour détecter les obstacles et les éviter
	 * @param wallToAvoid Mur à éviter
	 * @return renvoi le point d'évitement trouvé
	 */
	private PVector avoidWall(PVector walker, Wall wallToAvoid) {
		PVector walkerToLocation = PVector.sub(location, walker);
		walkerToLocation.normalize();

		// Normale
		PVector normal = new PVector(-walkerToLocation.y, walkerToLocation.x);

		// éloignement de l'obstacle
		normal.mult(wallToAvoid.getDiameter() / 2f + this.hitbox / 2f);

		PVector unitWallDirection = PVector.sub(wallToAvoid.getPosition(), walker);
		normalCalculation(walker, normal, unitWallDirection);

		// Si aucun des points d'évitements n'est valide, on débug l'unité en lui donnant un nouvel objectif
		if (this.ground.isValidLocation(walker, this.hitbox)) {
			PVector debugger = PVector.sub(this.location, this.position);
			debugger.normalize();
			debugger.mult(-1);

			PVector newLocation = new PVector(this.position.x, this.position.y);
			newLocation.add(debugger);

			this.location = newLocation;
		}

		return walker;
	}

	/**
	 * Met à jour l'endroit où se dirige l'unité
	 * @return renvoi l'endroit où aller
	 */
	private PVector findANewLocation() {
		Unit isNearAnUnit = this.ground.nearAnUnit(this.position, this.hitbox * 2);

		if (isFollowing) { // Si l'unité suit une autre unité
			// Peut arrêter de suivre (0.1% chance/tick soit 9%/frame)
			int continueFollowing =	ThreadLocalRandom.current().nextInt(0,1000);
			//System.out.println(continueFollowing);
			if (continueFollowing < 1) {
				this.isFollowing = false;
				boolean onlyFollower = true;

				for (Unit otherFollower : this.ground.getUnitList()) {
					if (otherFollower.unitFollowed == this.unitFollowed) {
						onlyFollower = false;
						break;
					}
				}

				if (onlyFollower) {
					this.unitFollowed.isFollowed = false;
				}

				this.unitFollowed = null;
				return newLocationGenerator();
			} else {
				this.unitFollowed.shade = shade;
				return unitFollowed.position;
			}
		} else { // Si l'unité ne suit aucune unité
			if (this.location.equals(new PVector(0, 0)) || this.position.dist(this.location) < 1f) { // Si l'unité à atteint son but
				return newLocationGenerator(); // trouver un nouveau endroit ou aller
			}

			// Si une unité est proche, que cette unité n'est pas suivie et que l'unité proche n'est pas ami
			if (isNearAnUnit != null && !isFollowed) {
				this.unitFollowed = isNearAnUnit;
				this.unitFollowed.isFollowed = true;
				this.isFollowing = true;
				return unitFollowed.position;
			}

			// S'il l'unité est suivi mais que les unités qui suivent sont trop éloignées
			if (isFollowed && isNearAnUnit == null) {
				this.isFollowed = false;

				for (Unit friend : this.ground.getUnitList()) {
					if (friend.unitFollowed == this) {
						friend.unitFollowed = null;
						friend.isFollowing = false;
					}
				}

				return newLocationGenerator();
			}
		}

		return this.location;
	}

	/**
	 * Trouve un nouvel endroit où aller
	 * @return renvoi l'endroit où doit aller l'unité ensuite
	 */
	private PVector newLocationGenerator() {
		PVector newLocationGenerator = new PVector(ThreadLocalRandom.current().nextInt((int) this.ground.getPosBegin().x + this.hitbox, (int) this.ground.getPosBegin().x + this.ground.getSizeX() - this.hitbox), ThreadLocalRandom.current().nextInt((int) this.ground.getPosBegin().y + this.hitbox, (int) this.ground.getPosBegin().y + this.ground.getSizeY() - this.hitbox));

		while (this.ground.isValidLocation(newLocationGenerator, this.hitbox)) {
			newLocationGenerator = new PVector(ThreadLocalRandom.current().nextInt((int) this.ground.getPosBegin().x + this.hitbox, (int) this.ground.getPosBegin().x + this.ground.getSizeX() - this.hitbox), ThreadLocalRandom.current().nextInt((int) this.ground.getPosBegin().y + this.hitbox, (int) this.ground.getPosBegin().y + this.ground.getSizeY() - this.hitbox));
		}

		return newLocationGenerator;
	}

	public void display() {
		Main.processing.noStroke();
		Main.processing.fill(this.shade, 90, 200);
		Main.processing.pushMatrix();
		Main.processing.translate(this.position.x, this.position.y);
		Main.processing.rotate(this.direction.heading());
		Main.processing.beginShape();
		Main.processing.vertex(15, 0);
		Main.processing.vertex(-7, 7);
		Main.processing.vertex(-7, -7);
		Main.processing.endShape(Main.processing.CLOSE);
		Main.processing.popMatrix();
	}

	public PVector getPosition() {
		return this.position;
	}

	public int getHitbox() {
		return this.hitbox;
	}

	public PVector getWaypoint() {
		return this.waypoint;
	}

	public PVector getLocation() {
		return this.location;
	}

	public float getShade() {
		return this.shade;
	}

	public boolean getIsFollowing() {
		return this.isFollowing;
	}

	public boolean getIsFollowed() {
		return this.isFollowed;
	}
}
