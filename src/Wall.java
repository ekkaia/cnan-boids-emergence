import processing.core.PVector;

public class Wall {
	private final PVector position;
	private final int diameter;

	/**
	 * Crée un mur : une zone de collision avec les unités
	 * @param position position du mur
	 * @param diameter diamètre du mur (cercle)
	 */
	public Wall(PVector position, int diameter) {
		this.position = position;
		this.diameter = diameter;
	}

	/**
	 * Affiche le mur
	 */
	public void display() {
		Main.processing.fill(229, 138, 138);
		Main.processing.strokeWeight(2);
		Main.processing.stroke(197, 44, 44);
		Main.processing.circle(this.position.x, this.position.y, this.diameter);
	}

	public PVector getPosition() {
		return this.position;
	}

	public int getDiameter() {
		return this.diameter;
	}
}
